package kr.codev.begg;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by k on 2017. 2. 4..
 */

public class EGGApp extends Application {
    private static EGGApp instance;

    public static EGGApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Realm.setDefaultConfiguration(getRealmConfig());
    }

    private RealmConfiguration getRealmConfig() {
        return new RealmConfiguration.Builder(this)
                .deleteRealmIfMigrationNeeded()
                .build();
    }
}
