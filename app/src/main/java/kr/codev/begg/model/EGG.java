package kr.codev.begg.model;

import io.realm.RealmObject;

/**
 * Created by k on 2017. 2. 5..
 */

public class EGG extends RealmObject {
    private Integer high;
    private Integer mid;
    private Long timestamp;
    private Float plot;
    private Float id;

    public Integer getHigh() {
        return high;
    }

    public void setHigh(Integer high) {
        this.high = high;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Float getPlot() {
        return plot;
    }

    public void setPlot(Float plot) {
        this.plot = plot;
    }

    public Float getId() {
        return id;
    }

    public void setId(Float id) {
        this.id = id;
    }

}
