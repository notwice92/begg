package kr.codev.begg.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmResults;
import kr.codev.begg.R;
import kr.codev.begg.model.EGG;

public class SplashActivity extends AppCompatActivity {


    private static final String outputFileName = "exampleOutput.csv";
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        setContentView(R.layout.activity_splash);

        findViewById(R.id.GRAPH).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashActivity.this, GraphActivity.class));
            }
        });

        findViewById(R.id.db_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.deleteAll();
                    }
                });
            }
        });

        findViewById(R.id.db_export).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateCsvFile(realm.where(EGG.class).findAll());
            }
        });
    }

    @Override
    protected void onDestroy() {
        try {
            realm.close();
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    //https://github.com/alexdao/realm2csv 참고
    private void generateCsvFile(RealmResults<EGG> eggs) {
        FileOutputStream outputStream;
        String columnHeader = "timestamp, high, mid, plot";
        String comma = ",";
        String newLine = "\n";

        try {
            outputStream = openFileOutput(outputFileName, Context.MODE_PRIVATE);
            outputStream.write(columnHeader.getBytes());
            outputStream.write(newLine.getBytes());

            for (EGG e : eggs) {
                outputStream.write(e.getTimestamp().toString().getBytes());
                outputStream.write(comma.getBytes());
                outputStream.write(e.getHigh().toString().getBytes());
                outputStream.write(comma.getBytes());
                outputStream.write(e.getMid().toString().getBytes());
                outputStream.write(comma.getBytes());
                outputStream.write(e.getPlot().toString().getBytes());
                outputStream.write(newLine.getBytes());
            }

            outputStream.flush();
            outputStream.close();
            Toast.makeText(this, "export 완료!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
